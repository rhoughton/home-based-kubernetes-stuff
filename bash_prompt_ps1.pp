node default {
  # Define a variable for the color codes
  $color_user = "\033[0;32m"  # Green color for user
  $color_reset = "\033[0m"   # Reset color

  exec { 'set_PS1':
    command => "export PS1='${color_user}\\u@\\h (\\033[1;35m\\h\\033[0m) \\w \\u${color_reset}'",
    path    => '/bin:/usr/bin',  # Specify the path to search for the command
    unless  => "test \"\$PS1\" = '${color_user}\\u@\\h (\\033[1;35m\\h\\033[0m) \\w \\u${color_reset}'",
  }

}
