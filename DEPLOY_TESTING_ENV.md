Open powershell, type 
```minikube start```
 then -> 
```
minikube service prometheus-server-np
```
now
```
minikube service grafana-np
```
for password generation
```
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo 
```
For anyone on Powershell use this instead:
```
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | ForEach-Object{ [Text.Encoding]::UTF8.GetString([Convert]::FromBase64String($_)) }
```
Next ensure the nginx ingress controller for minikube is on by checking with this
```
minikube addons list
```
if not use 
```
minikube addons enable ingress 
```
This controller is an API object that defines rules that allow external access to services in a cluster. 
verify that the NGINX Ingress controller is running by executing 
```
kubectl get pods -n ingress-nginx 
```
Note that it may take up to a minute before you see these pods running (this is also a helpful command: minikube addons list)


Misc useful commands:
```
Minikube dashboard
```
helm version
choco -v
```
Set-Alias -Name k -Value kubectl 
```
(if Loki isn’t already installed: helm install loki grafana/loki-stack (I was surprised it was this easy…)). 
Or you can choose from these: helm search repo loki
