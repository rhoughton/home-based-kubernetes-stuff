Install Helm for Rancher and its Dependencies: Helm is the package manager for Kubernetes. You can install it using Chocolatey
choco install kubernetes-helm 
I kept trying to -v or --version helm and it didn't work because the syntax doesn't want any tack - :( 
    ```
    helm --help
    ```
Deploy Rancher: Now you can deploy Rancher. You can run Rancher as a Docker container using the following command
```
docker run --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher:latest
```
Import Minikube into Rancher: After setting up Rancher, you can import your existing Minikube cluster into Rancher
```
minikube ip 
```
<details>

<summary> click me </summary>

Hello!

</details>