# This line defines a class named 'tree'
class tree {

  # This resource ensures the 'tree' package is installed
  package { 'tree':
    ensure => installed,
  }
}
#rpm -qa | grep tree
