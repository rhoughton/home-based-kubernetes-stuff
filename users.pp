$passwd_hash = ''
class users { 
  include users::groups
  user { 'RandolphScott':
    ensure => present,
    home => '/home/RandolphScott',
    shell => '/bin/bash',
    managehome => true,
    gid => 'puppetusers'
    password => $passwd_hash,
    }
}
#puppet apply --noop users.pp and/or init.pp 
#id newuser
#openssl passwd -1 password_you_choose