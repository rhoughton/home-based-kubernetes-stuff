# Install and configure promtail
class promtail {
  package { 'promtail':
    ensure => installed,
  }

  service { 'promtail':
    ensure => running,
    enable => true,
  }
}

# Install and configure loki
class loki {
  package { 'loki':
    ensure => installed,
  }

  service { 'loki':
    ensure => running,
    enable => true,
  }
}

#Apply the classes to the node (client) This line defines a node definition in Puppet. 
#The default keyword specifies that this definition applies to all nodes unless overridden by more specific definitions. 
node default {
  include promtail
  include loki
}
