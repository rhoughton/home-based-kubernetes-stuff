sudo puppet cert --generate <Client_Hostname>
sudo puppet cert --sign <Client_Hostname>
sudo puppet agent --test
sudo puppet agent -t

#is the certname the same as the hostname?
puppet config print certname

# /opt/puppetlabs and /etc/puppets
# agent config is in /etc/puppetlabs/puppet
# server config is in /etc/puppetlabs/puppetserver/conf.d

#to see if auditd is enabled/running look for audit=1 in this output
#this file contains the kernel cmd line parameters passed during boot
cat /proc/cmdline
#another approach is to check kernel messages
dmesg | grep "Command line"

#TFTP (Trivial File Transfer Protocol): is a simple protocol used for transferring files between servers and computers